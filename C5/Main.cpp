#include <iostream>
using namespace std;

#include "Tests.h"
#include "CifreNumar.h"
#include "Divizori.h"

int main() { // Se citesc val pana la 0.Minimul valorilor Prime
	int x, min;

	cin >> x;
	min = INT_MAX;
	while (x != 0) {
		if ((Prim(x))&&(x < min)) {
			min = x;
		}
		cin >> x;
	}

	cout << min;
}