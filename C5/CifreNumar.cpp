#include "CifreNumar.h"

#include <iostream>
using namespace std;


void AfisareCifre(int n)
{
	for (int t = n; t; t /= 10) {
		cout << t % 10 << endl;
	}
}

int SumaCifre(int n)
{
	int S = 0;
	for (int t = n; t; t /= 10) {
		S += t % 10 ;
	}
	return S;
}

int ProdusCifre(int n)
{
	int P = 1;
	for (int t = n; t; t /= 10) {
		P *= t % 10;
	}
	return P;
}

int CateCifrePare(int n)
{
	int ct = 0;
	for (int t = n; t; t /= 10) {
		int c = t % 10;
		if (c%2==0){
			ct++;
		}
	}
	return ct;
}

int MaximCifre(int n)
{
	int max = INT_MIN;
	for (int t = n; t; t /= 10) {
		int c = t % 10;
		if (c > max) {
			max = c;
		}
	}
	return max;
}

int MinimCifre(int n)
{
	int min = INT_MAX;
	for (int t = n; t; t /= 10) {
		int c = t % 10;
		if (c < min) {
			min = c;
		}
	}
	return min;
}

int Invers(int n)
{
	int m = 0;
	for (int t = n; t; t /= 10) {
		int c = t % 10;
		m = m * 10 + c;	}
	return m;
}

bool Palindrom(int n)
{
	return n == Invers(n);
}

int PrimaCifra(int n)
{
	while (n > 9) {
		n /= 10;
	}
	return n;
}

//
//M = {12,6,88,20,5}
//
//max=88
//
//max=-infinit
//pentru toate valorile x din M
//	if (x>max) {
//		max=x;
//	}
