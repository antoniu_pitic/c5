#include "Tests.h"
#include "Divizori.h"
#include "CifreNumar.h"
#include <assert.h>

void TestAll()
{
	assert(SumaCifre(0) == 0);
	assert(SumaCifre(1234) == 10);

	assert(ProdusCifre(1234) == 24);
	
	assert(CateCifrePare(6452) == 3);

	assert(MaximCifre(111) == 1);
	assert(MaximCifre(12364) == 6);

	assert(MinimCifre(111) == 1);
	assert(MinimCifre(3624) == 2);

	assert(Invers(123) == 321);

	assert(Palindrom(123) == false);
	assert(Palindrom(3773) == true);

	assert(PrimaCifra(872) == 8);

	assert(SumaDivizori(6) == 12);

	assert(Prim(6) == false);
	assert(Prim(7) == true);


	assert(Cmmdc(17, 25) == 1);
	assert(Cmmdc(28, 120) == 4);

}

